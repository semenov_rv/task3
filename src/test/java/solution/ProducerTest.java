package solution;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class ProducerTest {

    @Test
    void putWordsForQueue() {
        Stream<String> testData = Stream.of("Vasya", "Petya", "Петя", "Иван", "ёж", "sanФранциско", "Ёжик", "");
        List<String> expectedWords = Arrays.asList("петя", "иван", "ёжик");

        BlockingQueue<String> expectedQueue = new LinkedBlockingDeque<>(expectedWords);
        BlockingQueue<String> actualQueue = new LinkedBlockingDeque<>();
        Producer actualProducer = new Producer(actualQueue, Mockito.any());
        actualProducer.putWordsInQueue(testData);

        assertLinesMatch(expectedQueue.stream(), actualQueue.stream());
    }

    @Test
    void produceWordsForQueue() {
        String POISON_PILL  = Main.POISON_PILL;
        String testURL = "file:///D:/files/mars.txt/";
        List<String> expectedList = Arrays.asList("марсик", "сикмар", "рамкис", "кисмар", "петя", "вася", "ваяс", POISON_PILL);

        BlockingQueue<String> queue = new LinkedBlockingDeque<>();
        Producer producer = new Producer(queue, POISON_PILL);
        producer.produceWordsForQueue(testURL);
        System.out.println(queue);

        assertLinesMatch(expectedList, new ArrayList<>(queue));

    }
}