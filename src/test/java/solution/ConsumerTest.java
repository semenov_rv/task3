package solution;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingDeque;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class ConsumerTest {

    @Test
    void takeStringFromQueue() {
        List<String> expectedWords = Arrays.asList("петя", "иван", "ёжик");

        Set<String> expectedSet = new ConcurrentSkipListSet<>(expectedWords);
        Set<String> actualSet = new ConcurrentSkipListSet<>();
        BlockingQueue<String> actualQueue = new LinkedBlockingDeque<>(expectedWords);
        actualQueue.add(Main.POISON_PILL);
        Consumer consumer = new Consumer(actualQueue, actualSet, Mockito.any(), Main.POISON_PILL, 1);
        consumer.takeStringFromQueue();

        assertLinesMatch(expectedSet.stream(),actualSet.stream());
    }
}