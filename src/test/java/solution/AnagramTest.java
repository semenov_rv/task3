package solution;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class AnagramTest {

    @Test
    void findAnagram() {
        Stream<String> testData = Stream.of("вася", "атлант", "талант", "тантал", "крот");
        String testWord = "лантат";

        Map<String, List<String>> expectedMap = new HashMap<>();
        Map<String, List<String>> actualMap = new HashMap<>();
        expectedMap.put(testWord, Arrays.asList("атлант", "талант", "тантал"));
        Anagram anagram = new Anagram(Mockito.any(), Mockito.any(), actualMap);
        anagram.findAnagram(testData, testWord);
        List<String> expectedList = expectedMap.entrySet().stream().map(e -> e.getKey() + e.getValue()).collect(Collectors.toList());
        List<String> actualList = actualMap.entrySet().stream().map(e -> e.getKey() + e.getValue()).collect(Collectors.toList());
        System.out.println(expectedList);
        System.out.println(actualList);

        assertLinesMatch(expectedList, actualList);
    }
}