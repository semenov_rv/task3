package solution;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Consumer {
    private final BlockingQueue<String> blockingQueue;
    private final Set<String> setWordsForWordBook;
    private final Path outputWordBook;
    private boolean consume = true;
    private final String POISON_PILL;
    private final int POISON_PILL_COUNT;
    private int count ;

    public Consumer(BlockingQueue<String> blockingQueue, Set<String> setWordsForWordBook, Path outputWordBook, String POISON_PILL, int POISON_PILL_COUNT) {
        this.blockingQueue = blockingQueue;
        this.setWordsForWordBook = setWordsForWordBook;
        this.outputWordBook = outputWordBook;
        this.POISON_PILL = POISON_PILL;
        this.POISON_PILL_COUNT = POISON_PILL_COUNT;
    }

    public void makeWordBook() {
        System.out.println("Start writing wordbook to file.");
        try {
            Files.write(outputWordBook, setWordsForWordBook, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new ApplicationException("Error writing to file.");
        }
        System.out.println("\nsetWordsForWordBook size = " + setWordsForWordBook.size());
        System.out.println(Thread.currentThread().getName() + " Recorded words in the final file================================================\n");
    }

    public void takeStringFromQueue() {
        while (consume) {
            try {
                String wordFromQueue = blockingQueue.poll(2000, TimeUnit.MILLISECONDS);
                if (wordFromQueue != null) {
                    if (wordFromQueue.equals(POISON_PILL)) {
                        count++;
                    } else {
                        setWordsForWordBook.add(wordFromQueue);
                        System.out.println(Thread.currentThread().getName() + " took a word from the queue------> " + wordFromQueue);
                    }
                }
                if (count == POISON_PILL_COUNT) {
                    consume = false;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new ApplicationException("Thread " + Thread.currentThread().getName() + " is interrupted");
            }
        }
        System.out.println(Thread.currentThread().getName() + " is stopped\n");
    }
}
