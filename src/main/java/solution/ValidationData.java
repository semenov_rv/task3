package solution;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ValidationData {

    public void validateOutputFile(String outputFile) throws ValidateException {
        try {
            Files.deleteIfExists(Paths.get(outputFile));
        } catch (IOException e) {
           throw new ValidateException("File deletion error.");
        }
        if (outputFile.isEmpty()) {
            throw new ValidateException("You entered an empty target filename. Please enter a valid name.");
        }
    }

}
