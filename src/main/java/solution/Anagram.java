package solution;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Anagram {
    private final Path wordBook;
    private final Path outputAnagramFile;
    private final  Map<String, List<String>> mapAnagram ;

    public Anagram(Path wordBook, Path outputAnagramFile, Map<String, List<String>> mapAnagram) {
        this.wordBook = wordBook;
        this.outputAnagramFile = outputAnagramFile;
        this.mapAnagram = mapAnagram;
    }

    public void createFileWithAnagram() {
        System.out.println("Start find anagram...");
        try (Stream<String> lines = Files.lines(wordBook)) {
            lines.forEach(word -> {
                try {
                    findAnagram(Files.lines(wordBook), word);
                } catch (IOException e) {
                    throw new ApplicationException("Stream reading failed.");
                }
            });
        } catch (IOException e) {
            throw new ApplicationException("Unsuccessful attempt to read file.");
        }
        System.out.println("End find anagram...");
        writeAnagramToFile(mapAnagram);
    }

    protected void findAnagram(Stream<String> collectOfWords, String word) {
        final List<String> collectAnagrams = collectOfWords
                .filter(str -> !(str.equals(word)))
                .filter(string -> string.length() == word.length())
                .filter(string -> checkAnagram(string).equals(checkAnagram(word)))
                .collect(Collectors.toList());

        if (!(collectAnagrams.isEmpty())) {
            mapAnagram.put(word, collectAnagrams);
        }
    }

    private void writeAnagramToFile(Map<String, List<String>> mapAnagram) {
        System.out.println("Start write anagram to file...");
        List<String> anagramList = mapAnagram.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(Comparator.naturalOrder()))
                .map(elem -> String.format("%-20s : %s", elem.getKey(), elem.getValue()).replaceAll("[\\[\\]]", ""))
                .collect(Collectors.toList());
        try {
            Files.write(outputAnagramFile, anagramList, StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new ApplicationException("Failed to write anagrams to file.");
        }
    }

    private String checkAnagram(String string) {
        char[] chars = string.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}
