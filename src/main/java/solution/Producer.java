package solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Stream;

public class Producer {
    private final BlockingQueue<String> blockingQueue;
    private final String POISON_PILL ;

    public Producer(BlockingQueue<String> blockingQueue, String POISON_PILL) {
        this.blockingQueue = blockingQueue;
        this.POISON_PILL = POISON_PILL;
    }

    public void produceWordsForQueue(String urlFromList) {
        URL url;
        try {
            url = new URL(urlFromList);
            try (InputStreamReader inputStreamReader = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8);
                 BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
                putWordsInQueue(bufferedReader.lines());
            }
        } catch (IOException e) {
            throw new ApplicationException("Error in reading data from URL.");
        } finally {
            try {
               blockingQueue.put(POISON_PILL);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    protected void putWordsInQueue(Stream<String> lines) {
        lines.map(e -> e.split("[\\P{L}]+"))
                .flatMap(Arrays::stream)
                .map(String::toLowerCase)
                .filter(word -> word.matches("[а-яёА-ЯЁ]{3,}"))
                .forEach(word -> {
                    try {
                        blockingQueue.put(word);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        throw new ApplicationException("Thread " + Thread.currentThread().getName() + "is interrupted");
                    }
                });
    }

}

