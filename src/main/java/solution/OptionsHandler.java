package solution;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OptionsHandler {
    CommandLineParser parser = new DefaultParser();
    CommandLine commandLine = null;
    ValidationData validationData = new ValidationData();

    public void initOptions(String[] args) {
        Options options = new Options();
        final OptionsContainer[] values = OptionsContainer.values();

        for (OptionsContainer optionsContainer : values) {
            Option option = new Option(optionsContainer.getOpt(), optionsContainer.getLongOpt(), optionsContainer.isHasArg(), optionsContainer.getDescription());
            option.setRequired(optionsContainer.isRequired());
            options.addOption(option);
        }

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("\nError. You have entered incorrect command parameters.");
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("This program read a file, sort a words and write a wordbook in the file, which you specify. Then program find anagram\n" +
                    "and write in the new file all anagrams from a wordbook.\n" +
                    "\n" +
                    "Example use case:\n" +
                    "\n" +
                    "java -jar Task3.jar -i D:\\files3 -w \"C:\\Result.txt\" -a \"C:Anagram.txt\"", options);
            throw new ApplicationException(e.getMessage());
        }
    }

    public List<String> getListWithURI() {
        String fileName = null;
        List<String> uriList;
        if (commandLine.hasOption(OptionsContainer.INPUT.getOpt())) {
            fileName = commandLine.getOptionValue(OptionsContainer.INPUT.getOpt());
        }
        try (Stream<String> stringStream = Files.lines(Paths.get(fileName))) {
            uriList = stringStream
                    .map(String::trim)
                    .map(e -> e.split("\\s"))
                    .flatMap(Arrays::stream)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new ApplicationException("Reading error file");
        }
        return uriList;
    }

    public String getOutputFileToWordBook() {
        String outputFile = null;
        if (commandLine.hasOption(OptionsContainer.OUTPUT_WORDBOOK.getOpt())) {
            outputFile = commandLine.getOptionValue(OptionsContainer.OUTPUT_WORDBOOK.getOpt());
        }
        validationData.validateOutputFile(outputFile);
        return outputFile;
    }

    public String getOutputFileToAnagram() {
        String outputForAnagram = null;
        if (commandLine.hasOption(OptionsContainer.OUTPUT_ANAGRAM_FILE.getOpt())) {
            outputForAnagram = commandLine.getOptionValue(OptionsContainer.OUTPUT_ANAGRAM_FILE.getOpt());
        }
        validationData.validateOutputFile(outputForAnagram);
        return outputForAnagram;
    }
}
