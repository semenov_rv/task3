package solution;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;

public class Main {
   public static final int CAPACITY_QUEUE = 200;
   public static final String POISON_PILL = "POISON_PILL_TASK";

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        OptionsHandler optionsHandler = new OptionsHandler();
        optionsHandler.initOptions(args);
        final List<String> listWithURI = optionsHandler.getListWithURI();
        final int NUMBER_OF_THREADS  = listWithURI.size() * 2;   // * 2 means :  1 consumer and producer per URL
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        Map<String, List<String>> mapAnagram = new HashMap<>();
        try {
            BlockingQueue<String> blockingQueue = new LinkedBlockingDeque<>(CAPACITY_QUEUE);
            Set<String> setWordsForWordBook = new ConcurrentSkipListSet<>();
            final Path outputWordBook = Paths.get(optionsHandler.getOutputFileToWordBook());
            final Path anagramResultFile = Paths.get(optionsHandler.getOutputFileToAnagram());
            Producer producer = new Producer(blockingQueue, POISON_PILL);
            Consumer consumer = new Consumer(blockingQueue, setWordsForWordBook, outputWordBook, POISON_PILL, listWithURI.size());
            Anagram anagram = new Anagram(outputWordBook, anagramResultFile, mapAnagram);

            List<CompletableFuture<Void>> completableFutures = new ArrayList<>();
            for (String url : listWithURI) {
                completableFutures.add(CompletableFuture.runAsync(() -> producer.produceWordsForQueue(url), executorService));
                completableFutures.add(CompletableFuture.runAsync(consumer::takeStringFromQueue, executorService));
            }
            
            completableFutures.forEach(CompletableFuture::join);
            CompletableFuture.runAsync(consumer::makeWordBook, executorService).get();
            executorService.shutdown();
            anagram.createFileWithAnagram();
            System.out.println("Anagrams written to file : " + anagramResultFile);
        } catch (Exception e) {
            System.out.println("Error! The program terminates incorrectly!");
            System.out.println(e.getMessage());
            System.exit(1);
        } finally {
            executorService.shutdownNow();
        }
        System.out.println("\n*** Program running time : " + (System.currentTimeMillis() - startTime) / 1000 + " s ***");
    }
}

