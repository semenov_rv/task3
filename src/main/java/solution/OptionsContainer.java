package solution;

public enum OptionsContainer {
    INPUT("i", "inputFile", true, "File to read.", true),
    OUTPUT_WORDBOOK("w", "wordbook", true, "File to write.", true),
    OUTPUT_ANAGRAM_FILE("a", "anagram", true, "File to write.", true);

    private final String opt;
    private final String longOpt;
    private final boolean hasArg;
    private final String description;
    private final boolean required;

    OptionsContainer(String opt, String longOpt, boolean hasArg, String description, boolean required) {
        this.opt = opt;
        this.longOpt = longOpt;
        this.hasArg = hasArg;
        this.description = description;
        this.required = required;
    }

    public String getOpt() {
        return opt;
    }

    public String getLongOpt() {
        return longOpt;
    }

    public boolean isHasArg() {
        return hasArg;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }
}
