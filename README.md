This program read a file, sort a words and write a wordbook in the file, which you specify. Then program find anagram
and write in the new file all anagrams from a wordbook.

On the command line available 3 arguments:

1. "-i" URI from where the program will read the data.
2. "-w" output file for a wordbook.
3. "-a" output file for an anagrams from wordbook

Example use case:

java -jar Task3.jar -i D:\files3 -w "C:\Result.txt" -a "C:Anagram.txt"


